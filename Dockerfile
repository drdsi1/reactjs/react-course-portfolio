FROM rockylinux:9-minimal

RUN microdnf update -y && microdnf upgrade -y 

RUN microdnf install nodejs -y && npm install -g yarn

WORKDIR /app/react-portfolio

RUN npm install react react-dom -y && adduser nodejs

USER nodejs

EXPOSE 3000

