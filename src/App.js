import React, { Component } from 'react';
import Projects from './Projects';
import SocialProfiles from './SocialProfiles';
import profile from './assets/profile.png';

class App extends Component {
    state = { displayBio: false };

    toogleDisplayBio = () => {
        this.setState({ displayBio: !this.state.displayBio })
    }

    render() {
        const bio = this.state.displayBio ? (
            <div>
                <p>I live in Brazil, and code every day.</p>
                <p>My favorite language is Rust and similares.</p>
                <p>Besides coding, I also love animes and games.</p>
                <button onClick={this.toogleDisplayBio}>Show less</button>
            </div>
        ) : (
                <div>
                    <button onClick={this.toogleDisplayBio}>Read more</button>
                </div>
        );

        return (
            <div>
                <img src={profile} alt='profile' className='profile'/>
                <h1>Hello</h1>
                <p>My name é Eduardo Dicarte, I'm a software engineering</p>
                <p>Bla bla bla</p>
                {bio}
                <hr />
                <Projects />
                <hr />
                <SocialProfiles />
            </div>
        )
    }

}

export default App;